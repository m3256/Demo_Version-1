/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Builder.Builder;
import java.applet.AudioClip;
import java.awt.Image;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Timer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author Nicolas Andrade
 */
public class Ventana extends javax.swing.JFrame implements KeyListener, ActionListener {

    Builder builder=new Builder();
    
    static ArrayList<String> Componentes = new ArrayList<String>();
    public static JLabel Imagen = new JLabel();
    

    public void CorrerHilo() {
        Vhilo mh = new Vhilo();
        Thread nuevoh = new Thread(mh);
        nuevoh.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException exc) {
            System.out.println("Hilo principal interrumpido.");
        }
    }
    public void CorrerSonido(){
        AudioClip sonido;
        int n = Componentes.size()-1;
        sonido = java.applet.Applet.newAudioClip(getClass().getResource(Componentes.get(n)));
        sonido.play();
    }

    public void build(String instruccion, String opcion) {
        for (int i = 0; i < builder.Builder(opcion,instruccion).size(); i++) {
            Componentes.add(builder.Builder(opcion,instruccion).get(i));
        }
    }

    public Ventana() {

        initComponents();
        this.setBounds(0, 0, 500, 500);

        Imagen.setBounds(50, 50, 200, 200);
        getContentPane().add(Imagen);
        addKeyListener(this);
        setFocusable(false);

    }

    public void actionPerformed(ActionEvent e) {

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Elfo", "Humano", "Orco" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });
        jComboBox1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox1KeyPressed(evt);
            }
        });
        getContentPane().add(jComboBox1);
        jComboBox1.setBounds(10, 20, 91, 20);

        jLabel1.setText("Movimientos");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(730, 30, 90, 14);

        jLabel2.setText("Flecha arriba: Salto");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(740, 60, 130, 14);

        jLabel3.setText("Flecha izquierda: Izquierda");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(740, 80, 150, 14);

        jLabel4.setText("Flecha derecha: Derecha");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(740, 100, 150, 14);

        jLabel5.setText("Supr: Morir");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(740, 120, 130, 14);

        jLabel6.setText("x: Ataque");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(740, 140, 100, 14);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed

    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jComboBox1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox1KeyPressed
        keyPressed(evt);
    }//GEN-LAST:event_jComboBox1KeyPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    // End of variables declaration//GEN-END:variables

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        String instruccion;
        String peronaje;
        peronaje = (String) jComboBox1.getSelectedItem();
        Componentes.clear();
        int c = e.getKeyCode();
        
        if (c == KeyEvent.VK_UP) {
        instruccion = "salto";
            this.build(instruccion, peronaje);
            this.CorrerHilo();
            this.CorrerSonido();
            
        } else if (c == KeyEvent.VK_LEFT) {
            instruccion = "izquierda";
            this.build(instruccion, peronaje);
            this.CorrerHilo();
            this.CorrerSonido();
            
        } else if (c == KeyEvent.VK_RIGHT) {
            instruccion = "derecha";
            this.build(instruccion, peronaje);
            this.CorrerHilo();
           this.CorrerSonido();
           
        } else if (c == KeyEvent.VK_X) {
            instruccion = "ataque";
            this.build(instruccion, peronaje);
            this.CorrerHilo();
            this.CorrerSonido();
        } else if (c == KeyEvent.VK_DELETE) {
            instruccion = "muerte";
            this.build(instruccion, peronaje);
            this.CorrerHilo();
            this.CorrerSonido();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

}
