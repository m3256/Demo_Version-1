package Builder;

public class Director
{
    public Director() {
    }
    // --------------------------
     public void construir( BuilderPersonaje builder )
    {
        builder.crearNuevoPersonaje();
        builder.construirDerecha();
        builder.construirIzquierda();
        builder.construirSalto();
        builder.construirMuerte();
        builder.construirAtaque();
    }
}
