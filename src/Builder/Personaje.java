package Builder;

import java.util.ArrayList;

public class Personaje
{
    ArrayList<String> derecha = new ArrayList<String>();
    ArrayList<String> izquierda = new ArrayList<String>();
    ArrayList<String> salto = new ArrayList<String>();
    ArrayList<String> muerte = new ArrayList<String>();
    ArrayList<String> ataque = new ArrayList<String>();
    // -------------------------------------------
    public Personaje() {
        
    }
    // -------------------------------------------
    public ArrayList<String>getDerecha() {
        return this.derecha;
    }
    // -------------------------------------------
    public void setDerecha(ArrayList<String> der) {
        this.derecha = der;
    }
    // -------------------------------------------
    public ArrayList<String> getIzquierda() {
        return this.izquierda;
    }
    // -------------------------------------------
    public void setIzquierda(ArrayList<String> izq) {
        this.izquierda = izq;
    }
    // -------------------------------------------
    public ArrayList<String> getSalto() {
        return salto;
    }
    // -------------------------------------------
    public void setSalto(ArrayList<String> sal) {
        this.salto = sal;
    }
    // -------------------------------------------
    public ArrayList<String> getMuerte() {
        return muerte;
    }
    // -------------------------------------------
    public void setMuerte(ArrayList<String> muer) {
        this.muerte = muer;
    }
    public ArrayList<String> getAtaque() {
        return ataque;
    }
    // -------------------------------------------
    public void setAtaque(ArrayList<String> atac) {
        this.ataque = atac;
    }
   
}