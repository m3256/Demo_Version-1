package Builder;

import java.util.ArrayList;

public class Builder
{
    String Instruccion;  
   ArrayList<String> VACIO = new ArrayList<String>();
    
    public ArrayList<String> Builder(String perso,String instruccion)
    {
        // Crear el objeto Director
         Director objFabrica = new Director();
        // Crear los objetos ConcreteBuilder
         BuilderPersonaje per = new ConstructorMovimientos(perso);
        
         
        objFabrica.construir(per);
        Personaje personaje = per.getPersonaje();
        Instruccion=instruccion;
        System.out.println(Instruccion);
        switch (Instruccion) {
            case "salto":
                return mostrarSalto(personaje );
            case "derecha":
               return mostrarDerecha(personaje);
            case "izquierda":
                return mostrarIzquierda(personaje);
            case "muerte":
                return mostrarMuerte(personaje);
            case "ataque":
                return mostrarAtaque(personaje);
               
        }
     return VACIO;
    }
    // --------------------------------
     public static ArrayList<String> mostrarDerecha( Personaje personaje )
    {
        ArrayList<String> mov = new ArrayList<String>();
        for(int i=0;i<personaje.getDerecha().size();i++)
        mov.add(personaje.getDerecha().get(i));
        return mov;
    }
      public static ArrayList<String> mostrarIzquierda( Personaje personaje )
    {
         ArrayList<String> mov = new ArrayList<String>();
        for(int i=0;i<personaje.getIzquierda().size();i++)
        mov.add(personaje.getIzquierda().get(i));
        return mov;
    }
    public static ArrayList<String> mostrarSalto( Personaje personaje )
    {
         ArrayList<String> mov = new ArrayList<String>();
        for(int i=0;i<personaje.getSalto().size();i++)
        mov.add(personaje.getSalto().get(i));
        return mov;
    }
    
      public static ArrayList<String> mostrarMuerte( Personaje personaje )
    {
         ArrayList<String> mov = new ArrayList<String>();
        for(int i=0;i<personaje.getMuerte().size();i++)
        mov.add(personaje.getMuerte().get(i));
        return mov;
    }
         public static ArrayList<String> mostrarAtaque( Personaje personaje )
    {
         ArrayList<String> mov = new ArrayList<String>();
        for(int i=0;i<personaje.getAtaque().size();i++)
        mov.add(personaje.getAtaque().get(i));
        return mov;
    }
}