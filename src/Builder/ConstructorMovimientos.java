package Builder;
import AbstractFactory.Ataque;
import AbstractFactory.Derecha;
import AbstractFactory.DerechaHumano;
import AbstractFactory.FabricaAbstracta;
import AbstractFactory.FabricaElfos;
import AbstractFactory.FabricaHumanos;
import AbstractFactory.FabricaOrcos;
import AbstractFactory.Izquierda;
import AbstractFactory.IzquierdaHumano;
import AbstractFactory.Muerte;
import AbstractFactory.Salto;
import java.util.ArrayList;
import java.util.Scanner;
public class ConstructorMovimientos extends BuilderPersonaje
{
    FabricaAbstracta fabrica;
    Salto salto;
    Izquierda izquierda;
    Derecha derecha;
    Ataque ataque;
    Muerte muerte;
    Scanner escaner;
    String Opcion;
    
    ArrayList<String> Derecha = new ArrayList<String>();
    ArrayList<String> Izquierda = new ArrayList<String>();
    ArrayList<String> Salto = new ArrayList<String>();
    ArrayList<String> Ataque = new ArrayList<String>();
    ArrayList<String> Muerte = new ArrayList<String>();
    
    public ConstructorMovimientos(String opcion) {
        
          Opcion = opcion;
        switch (Opcion) {
            case "Humano":
                fabrica = new FabricaHumanos();
                break;
            case "Elfo":
                fabrica = new FabricaElfos();
                break;
            case "Orco":
                fabrica = new FabricaOrcos();
                break;
        }
        
        salto = fabrica.crearSalto();
        izquierda = fabrica.crearIzquierda();
        derecha = fabrica.crearDerecha();
        ataque = fabrica.crearAtaque();
        muerte = fabrica.crearMuerte();
        
    }
    // ------------------------------
    @Override
     public void construirIzquierda() {
        Izquierda=izquierda.operacion();
        this.personaje.setIzquierda(Izquierda);
    }
    // ------------------------------
    @Override
     public void construirDerecha() { 
        Derecha=derecha.operacion();
        this.personaje.setDerecha(Derecha);
    }
    // ------------------------------
    @Override
     public void construirSalto() {
        Salto=salto.operacion();
        this.personaje.setSalto(Salto);
    }
    // ------------------------------
    @Override
     public void construirMuerte() {
        Muerte=muerte.operacion();
        this.personaje.setMuerte(Muerte);
    }
    @Override
     public void construirAtaque() {
        Ataque=ataque.operacion();
        this.personaje.setAtaque(Ataque);
    } 
}