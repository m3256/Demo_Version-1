package Builder;

import AbstractFactory.FabricaAbstracta;
import AbstractFactory.FabricaHumanos;


public abstract class BuilderPersonaje
{
     protected Personaje personaje;
    // ------------------------------
    public Personaje getPersonaje() {
        return this.personaje;
    }
    // ------------------------------
     public void crearNuevoPersonaje() {
        this.personaje = new Personaje();
    }
    // ------------------------------------
    // Métodos que deberán ser construídos por las clases que hereden de ésta
     public abstract void construirDerecha();
     public abstract void construirIzquierda();
     public abstract void construirSalto();
     public abstract void construirMuerte();
     public abstract void construirAtaque();
}