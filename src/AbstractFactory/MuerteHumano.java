/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory;

import java.util.ArrayList;

/**
 *
 * @author famil
 */
public class MuerteHumano implements Muerte {
    
     @Override
     public ArrayList<String> operacion() {
        ArrayList<String> MuerHum = new ArrayList<String>();
        MuerHum.add("/ImagenesHumano/MuerHum1.jpg");
        MuerHum.add("/ImagenesHumano/MuerHum2.jpg");
        MuerHum.add("/ImagenesHumano/MuerHum3.jpg");
        MuerHum.add("/ImagenesHumano/MuerHum4.jpg");
        MuerHum.add("/ImagenesHumano/MuerHum5.jpg");
        MuerHum.add("/ImagenesHumano/MuerHum6.jpg");
        MuerHum.add("/ImagenesHumano/MuerHum7.jpg");
        MuerHum.add("/Sonidos/HumanoMuerte.wav");
        return MuerHum;
    }
}
