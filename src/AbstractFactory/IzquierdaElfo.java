/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory;

import java.util.ArrayList;

/**
 *
 * @author estudiantes
 */
public class IzquierdaElfo implements Izquierda {

    @Override
     public ArrayList<String> operacion() {
         ArrayList<String> IzqElf = new ArrayList<String>();
        IzqElf.add("/ImagenesElfo/ElfIzq1.jpg");
        IzqElf.add("/ImagenesElfo/ElfIzq2.jpg");
        IzqElf.add("/ImagenesElfo/ElfIzq3.jpg");
        IzqElf.add("/ImagenesElfo/ElfIzq4.jpg");
        IzqElf.add("/ImagenesElfo/ElfIzq5.jpg");
        IzqElf.add("/ImagenesElfo/ElfIzq6.jpg");
        IzqElf.add("/Sonidos/Movimiento.wav");
        return IzqElf;
    }
}
